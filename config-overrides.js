// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const {
  injectBabelPlugin,
  getBabelLoader,
  getLoader,
  loaderNameMatches
} = require('react-app-rewired')
const webpack = require('webpack')
// const rewireCssModules = require('react-app-rewire-less-with-modules')
const path = require('path')

const eslintLoaderMatcher = function (rule) {
  return loaderNameMatches(rule, 'eslint-loader')
}

module.exports = function override (config, env) {
  // config = rewireCssModules(config, env, {
  //   modifyVars: {
  //     'layout-header-background': '#1a237e',
  //     'menu-dark-submenu-bg': '#283593',
  //     'layout-trigger-background': '#283593'
  //   }
  // })
  const babelLoader = getBabelLoader(config.module.rules)

  const eslintLoader = getLoader(config.module.rules, eslintLoaderMatcher)

  // babelLoader.options = require('babel-preset-react-native-stage-0/decorator-support')
  // config = injectBabelPlugin(
  //   ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }],
  //   config
  // )
  const pwd = path.resolve()

  babelLoader.include = [path.normalize(`${pwd}/src`)]

  // console.log(babelLoader)
  babelLoader.options.babelrc = true
  // eslintLoader.options.useEslintrc = true
  // var commonsChunkPluginCommon = new webpack.optimize.CommonsChunkPlugin({
  //   name: 'common',
  //   minChunks: 2
  // })
  // var commonsChunkPluginVendor = new webpack.optimize.CommonsChunkPlugin({
  //   name: 'vendor',
  //   minChunks: function (module) {
  //     return module.context && module.context.indexOf('node_modules') !== -1
  //   }
  // })
  // 顺序很重要
  if (env === 'production') {
    config.devtool = undefined
    // config.plugins.push(commonsChunkPluginVendor)
    // config.plugins.push(commonsChunkPluginCommon)
  }
  if (env === 'production' && process.env.ANALYZE_ENV) {
    config.plugins.push(new BundleAnalyzerPlugin({ generateStatsFile: true }))
    return config
  }
  // console.log(config.module.rules)
  // console.log(eslintLoader)
  console.log(config)
  config.devtool = 'eval-source-map'
  return config

}
