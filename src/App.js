import React, { Component } from 'react';
import './App.css';
import { connect } from './store';

const getAddAction = value => {
  return { type: 'ADD', payload: value };
};

const getAsyncAddAction = (dispatch, getState) => {
  setTimeout(() => {
    dispatch({
      type: 'ADD',
      payload: 1
    });
  }, 1000);
};
@connect(({ reducer }) => ({
  value: reducer.value,
  count: reducer.count
}))
class App extends Component {
  render() {
    const { store, dispatch } = this.props;
    console.log(this.props);
    console.log('render', store);
    return (
      <div className="App">
        <h5>value: {store.value}</h5>
        <h5>count: {store.count}</h5>
        <button onClick={() => dispatch(getAddAction(1))}>Add 1</button>
        <button onClick={() => dispatch(getAsyncAddAction)}>Async Add 1</button>
        <button onClick={() => dispatch({ type: 'MINUS', payload: 1 })}>Minus 1</button>
        <button onClick={() => dispatch({ type: 'CLEAR' })}>Clear all</button>
      </div>
    );
  }
}

// export default connect(({ value, count }) => ({ value, count }))(App);
export default App;
