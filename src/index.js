import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider, store } from './store';

console.log(App);
console.log(store, 555);

const render = () => {
  ReactDOM.render(
    <Provider value={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  );
};

if (module.hot) {
  module.hot.accept(App, () => {
    render();
  });
}
render();
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
