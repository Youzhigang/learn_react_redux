import { createStore, applyMiddleware, combineReducers, bindActionCreator } from './redux';
import React, { createContext } from 'react';
import thunk from './thunk';
import { createLogger } from 'redux-logger';

const defaultState = { value: 0, count: 0 };

function simpleLogger(store) {
  return function(next) {
    return function(action) {
      console.log('-----action', action.type);
      console.log('-----before state', store.getState());
      next(action);
      console.log('-----next state', store.getState());
    };
  };
}

const reducer = (state = defaultState, action) => {
  console.log(state, action);
  let { count, value } = state;
  switch (action.type) {
    case 'INIT':
      return { ...defaultState };
    case 'ADD':
      return { count: ++count, value: value + action.payload };
    case 'MINUS':
      return { count: ++count, value: value - action.payload };
    case 'CLEAR':
      return { ...defaultState };
    case 'ASYNC_ADD':
      return { ...defaultState };
    default:
      return state;
  }
};

export const store = createStore(
  combineReducers({ reducer }),
  compose(
    applyMiddleware(simpleLogger, thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);
export const StoreContext = createContext(store);

export function connect(fn) {
  return function(Target) {
    return class extends React.Component {
      render() {
        return (
          <StoreContext.Consumer>
            {store => {
              return <Target dispatch={store.dispatch} store={fn(store.getState())} />;
            }}
          </StoreContext.Consumer>
        );
      }
    };
  };
}

export class Provider extends React.Component {
  constructor(props) {
    super(props);
    this.store = this.props.value; // 完整的store
    this.onChange = this.onChange.bind(this);
    this.state = this.getOwnState();
  }

  componentDidMount() {
    this.store.subscribe(this.onChange);
  }

  onChange() {
    console.log('on change');
    this.setState(this.getOwnState());
  }

  componentWillUnmount() {
    this.store.unsubscribe(this.onChange);
  }

  getOwnState() {
    return this.store;
  }

  render() {
    return <StoreContext.Provider value={this.state}>{this.props.children}</StoreContext.Provider>;
  }
}

export function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg;
  }
  if (funcs.length === 1) {
    return funcs[0];
  }
  return funcs.reduce((preValue, fn) => {
    return (...args) => preValue(fn(...args));
  });
}
