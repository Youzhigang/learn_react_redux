// function createThunkMiddleware(extraArgument) {
//   return ({ dispatch, getState }) => next => action => {
//     if (typeof action === 'function') {
//       return action(dispatch, getState, extraArgument);
//     }

//     return next(action);
//   };
// }

function createThunkMiddleware(extraArgument) {
  return function(store) {
    return function(next) {
      // next 实际上就是 dispatch function
      return function(action) {
        console.log('thunk action', action);
        if (typeof action === 'function') {
          return action(store.dispatch, store.getState, extraArgument);
        }
        return next(action);
      };
    };
  };
}

const thunk = createThunkMiddleware();
thunk.withExtraArgument = createThunkMiddleware;

const _thunk = function(store) {
  return function(next) {
    return function(action) {
      if (typeof action === 'function') {
        return action(store.dispatch, store.getState);
      } else {
        next(action);
      }
    };
  };
};

/* 
1. 输入 store, 返回 需要 输入 next 的 function  // next 实际上就是 dispatch function
2. 输入 next , 返回需要输入 action 的 function
3. 输入 action, 执行一些逻辑, 返回 next 执行 action的结果(经过这个middleware 后更新的store)

middleware是对dispatch的enhancer, 改变dispatch的行为
*/
export default thunk;
